import { ApiModelProperty } from '@nestjs/swagger';

export class AuthSignInDto {
  @ApiModelProperty({description: 'User email'})
  readonly email: string;

  @ApiModelProperty({description: 'User password'})
  readonly password: string;
}

export class AuthSignUpDto {
  @ApiModelProperty({description: 'User email'})
  readonly email: string;

  @ApiModelProperty({description: 'User name'})
  readonly name: string;

  @ApiModelProperty({description: 'User password'})
  readonly password: string;
}