import { Controller, Body, Post } from '@nestjs/common';
import { ApiUseTags, ApiOperation } from '@nestjs/swagger';
import { AuthSignInDto, AuthSignUpDto } from './auth.dto';
import { UsersService } from '../users/users.service';

@Controller('auth')
@ApiUseTags('Authentication')
export class AuthController {
  constructor(private readonly usersService: UsersService) {}

  @Post('sign-in')
  @ApiOperation({title: 'Sign in user'})
  async signIn(@Body() credentials: AuthSignInDto) {
    return this.usersService.signIn(credentials);
  }

  @Post('sign-up')
  @ApiOperation({title: 'Sign up user'})
  async signUp(@Body() credentials: AuthSignUpDto) {
    return this.usersService.signUp(credentials);
  }
}
