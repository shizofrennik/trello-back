import { Controller, Get, Post, Put, Delete, Body, Param, UseGuards, Request } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags, ApiOperation } from '@nestjs/swagger';
import { ColumnsDto } from './columns.dto';
import { ColumnsService } from './columns.service';
import { CardsDto } from '../cards/cards.dto';
import { IsColumnOwner } from '../common/guards';
import { Owner } from '../common/reflectors';

@Controller('columns')
@ApiBearerAuth()
@ApiUseTags('columns')
@UseGuards(AuthGuard(), IsColumnOwner)
export class ColumnsController {
  constructor(private readonly columnsService: ColumnsService) {}

  @Get()
  @ApiOperation({title: 'Get all columns'})
  async findColumns(@Request() req) {
    return this.columnsService.findUserColumns(req.user.id);
  }

  @Get(':columnId')
  @ApiOperation({title: 'Get column by id'})
  @Owner(true)
  async findById(@Param('columnId') columnId) {
    return await this.columnsService.find(columnId)
  }

  @Put(':columnId')
  @ApiOperation({title: 'Update column by id'})
  @Owner(true)
  async update(@Param('columnId') columnId, @Body() columnsDto: ColumnsDto) {
    return await this.columnsService.update(columnId, columnsDto)
  }

  @Post()
  @ApiOperation({title: 'Create new column'})
  async create(@Body() columnsDto: ColumnsDto, @Request() request) {
    const entity = {...columnsDto, user: request.user.id};
    return await this.columnsService.create(entity);
  }

  @Post(':columnId/cards')
  @ApiOperation({title: 'Create card for specified column'})
  async createCard(@Param('columnId') columnId, @Body() cardsDto: CardsDto, @Request() request) {
    return await this.columnsService.addColumnCard(columnId, cardsDto);
  }

  @Delete(':columnId')
  @ApiOperation({title: 'Delete column by id'})
  @Owner(true)
  delete( @Param('columnId') columnId) {
    return this.columnsService.deleteOne(columnId);
  }
}
