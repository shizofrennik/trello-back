import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Columns } from './columns.entity';
import { ColumnsDto } from './columns.dto';
import { Cards } from '../cards/cards.entity';
import { CardsDto } from '../cards/cards.dto';

@Injectable()
export class ColumnsService {
  constructor(
    @InjectRepository(Columns)
    private readonly columnsRepository: Repository<Columns>,
    @InjectRepository(Cards)
    private readonly cardsRepository: Repository<Cards>,
  ) {}

  async findUserColumns(user: number): Promise<Columns[]> {
    try {
      return await this.columnsRepository.find({ where: { user }});
    } catch (err) {
      return err;
    }
  }

  async find(id: number): Promise<Columns> {
    try {
      return await this.columnsRepository.findOne(id);
    } catch (err) {
      return err;
    }
  }

  async update(columnId: number, entry: ColumnsDto): Promise<Columns> {
    try {
      await this.columnsRepository.update(columnId, {...entry, id: columnId});
      return this.columnsRepository.findOne(columnId);
    } catch (err) {
      return err;
    }
  }

  async create(entry: ColumnsDto): Promise<Columns> {
    try {
      return await this.columnsRepository.save({...entry, id: null});
    } catch (err) {
      return err;
    }
  }

  async deleteOne(columnId: string) {
    try {
      return await this.columnsRepository.delete(columnId);
    } catch (err) {
      return err;
    }
  }

  async addColumnCard(columnId: number, entry: CardsDto): Promise<Cards> {
    try {
      const column = await this.columnsRepository.findOne(columnId);
      return await this.cardsRepository.save({...entry, column, id: null});
    } catch (err) {
      return err;
    }
  }
}