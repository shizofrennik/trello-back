import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ColumnsService } from './columns.service';
import { ColumnsController } from './columns.controller';
import { Columns } from './columns.entity';
import { Cards } from '../cards/cards.entity';
import { CardsService } from '../cards/cards.service';
import { Comments } from '../comments/comments.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Columns, Cards, Comments])],
  providers: [ColumnsService, CardsService],
  controllers: [ColumnsController],
  exports: [ColumnsService]
})
export class ColumnsModule {}