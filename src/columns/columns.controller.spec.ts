import { Test, TestingModule } from '@nestjs/testing';
import { ColumnsController } from './columns.controller';

describe('Columns Controller', () => {
  let module: TestingModule;
  
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [ColumnsController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: ColumnsController = module.get<ColumnsController>(ColumnsController);
    expect(controller).toBeDefined();
  });
});
