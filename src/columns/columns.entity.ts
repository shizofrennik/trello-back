import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany, RelationId } from 'typeorm';
import { Users } from '../users/users.entity';
import { Cards } from '../cards/cards.entity';

@Entity()
export class Columns {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 100 })
  title: string;

  @ManyToOne(type => Users, user => user.columns, { onDelete: 'CASCADE' })
  user: Users;

  @OneToMany(type => Columns, column => column.cards)
  cards: Cards[];

  @RelationId((column: Columns) => column.user)
  userId: number;
}