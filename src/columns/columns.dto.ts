import { ApiModelProperty } from '@nestjs/swagger';
import { Users } from '../users/users.entity';
import { Cards } from '../cards/cards.entity';

export class ColumnsDto {
  readonly id: number;

  @ApiModelProperty()
  readonly title: string;

  @ApiModelProperty()
  readonly description: string;

  readonly user: Users;
  readonly cards: Cards[];
  readonly userId: number;
}