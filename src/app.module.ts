import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { ColumnsController } from './columns/columns.controller';
import { UsersController } from './users/users.controller';
import { CardsController } from './cards/cards.controller';
import { AppService } from './app.service';
import { ColumnsModule } from './columns/columns.module';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { CardsModule } from './cards/cards.module';
import { CommentsModule } from './comments/comments.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    ColumnsModule,
    AuthModule,
    UsersModule,
    CardsModule,
    CommentsModule,
  ],
  controllers: [
    AppController,
    ColumnsController,
    UsersController,
    CardsController
  ],
  providers: [AppService],
})
export class AppModule {}
