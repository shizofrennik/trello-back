import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Behold the mighty of PURRWEB!';
  }
}
