import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CardsController } from './cards.controller';
import { CardsService } from './cards.service';
import { ColumnsService } from '../columns/columns.service';
import { CommentsService } from '../comments/comments.service';
import { Cards } from './cards.entity';
import { Columns } from '../columns/columns.entity';
import { Comments } from '../comments/comments.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Cards, Columns, Comments])],
  controllers: [CardsController],
  providers: [CardsService, ColumnsService, CommentsService],
  exports: [CardsService]
})
export class CardsModule {}
