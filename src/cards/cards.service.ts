import { Injectable, ExecutionContext } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, In } from 'typeorm';
import { Cards } from './cards.entity';
import { CardsDto } from './cards.dto';
import { Users } from '../users/users.entity';
import { Columns } from '../columns/columns.entity';
import { Comments } from '../comments/comments.entity';
import { CommentsDto } from '../comments/comments.dto';

@Injectable()
export class CardsService {
  constructor(
    @InjectRepository(Cards)
    private readonly cardsRepository: Repository<Cards>,
    @InjectRepository(Columns)
    private readonly columnsRepository: Repository<Columns>,
    @InjectRepository(Comments)
    private readonly commentsRepository: Repository<Comments>,
  ) {}

  async findUserCards(user: Users) {
    const columns = await this.columnsRepository.find({ where: { user }});
    const columnsIds = columns.map(c => c.id);
    return await this.cardsRepository.find({ where: { column: In(columnsIds)}});
  }

  async findColumnCards(column: number): Promise<Cards[]> {
    try {
      return await this.cardsRepository.find({ where: { column }});
    } catch (err) {
      return err;
    }
  }

  async find(cardId: number): Promise<Cards> {
    try {
      return await this.cardsRepository.findOne(cardId);
    } catch (err) {
      return err;
    }
  }

  async update(cardId: number, entry: CardsDto): Promise<Cards> {
    try {
      await this.cardsRepository.update(cardId, {...entry, id: cardId});
      return await this.cardsRepository.findOne(cardId);
    } catch (err) {
      return err;
    }
  }

  async create(entry: CardsDto): Promise<Cards> {
    try {
      return await this.cardsRepository.save({...entry, id: null});
    } catch (err) {
      return err;
    }
  }

  async deleteOne(cardId: string) {
    try {
      return await this.cardsRepository.delete(cardId);
    } catch (err) {
      return err;
    }
  }

  async addCommentToCard(cardId: number, entry: CommentsDto, user: Users): Promise<Comments> {
    try {
      const card = await this.cardsRepository.findOne(cardId);
      return await this.commentsRepository.save({...entry, card, user, id: null});
    } catch (err) {
      return err;
    }
  }
}