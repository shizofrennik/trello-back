import { Controller, Get, Post, Put, Delete, Body, Param, UseGuards, Request, UsePipes } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags, ApiOperation } from '@nestjs/swagger';
import { CardsDto } from './cards.dto';
import { CommentsDto } from '../comments/comments.dto';
import { CardsService } from './cards.service';
import { IsCardOwner } from '../common/guards';
import { Owner } from '../common/reflectors';
import { ReqPipe } from '../common/pipes';

@Controller('cards')
@ApiBearerAuth()
@ApiUseTags('cards')
@UseGuards(AuthGuard(), IsCardOwner)
export class CardsController {
  constructor(private readonly cardsService: CardsService) {}

  @Get()
  @ApiOperation({title: 'Get all cards'})
  async findColumns(@Request() req) {
    return this.cardsService.findUserCards(req.user);
  }

  @Get(':cardId')
  @ApiOperation({title: 'Get card by id'})
  @Owner(true)
  async findById(@Param('cardId', ReqPipe) cardId) {
    return await this.cardsService.find(cardId)
  }

  @Put(':cardId')
  @ApiOperation({title: 'Update card by id'})
  @Owner(true)
  async update(@Param('cardId') cardId, @Body() cardsDto: CardsDto) {
    return await this.cardsService.update(cardId, cardsDto)
  }

  @Post()
  @ApiOperation({title: 'Create card'})
  async create(@Body() cardsDto: CardsDto) {
    return await this.cardsService.create(cardsDto);
  }

  @Post(':cardId/comments')
  @ApiOperation({title: 'Create comment for specified card'})
  async createCard(@Param('cardId') cardId, @Body() commentsDto: CommentsDto, @Request() request) {
    return await this.cardsService.addCommentToCard(cardId, commentsDto, request.user);
  }

  @Delete(':cardId')
  @ApiOperation({title: 'Delete card by id'})
  @Owner(true)
  delete( @Param('cardId') cardId) {
    return this.cardsService.deleteOne(cardId);
  }
}
