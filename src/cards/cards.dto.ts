import { ApiModelProperty } from '@nestjs/swagger';
import { Columns } from '../columns/columns.entity';
import { Comments } from '../comments/comments.entity';

export class CardsDto {
  readonly id: number;

  @ApiModelProperty()
  readonly title: string;

  @ApiModelProperty()
  readonly description: string;

  @ApiModelProperty()
  readonly checked: boolean;

  @ApiModelProperty()
  readonly column: Columns;

  readonly columnId: number;
  readonly comments: Comments[];
  readonly commentsIds: number[];
}