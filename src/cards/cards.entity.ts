import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany, RelationId } from 'typeorm';
import { Columns } from '../columns/columns.entity';
import { Comments } from '../comments/comments.entity';

@Entity()
export class Cards {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 100 })
  title: string;

  @Column('text')
  description: string;

  @Column({ nullable: true, default: false })
  checked: boolean;

  @ManyToOne(type => Columns, column => column.cards, { onDelete: 'CASCADE' })
  column: Columns

  @RelationId((card: Cards) => card.column)
  columnId: number;

  @OneToMany(type => Comments, comment => comment.card)
  comments: Comments[];

  @RelationId((card: Cards) => card.comments)
  commentsIds: number[];
}