export interface SignUpInterface {
  email: string;
  password: string;
  name: string;
}

export interface SignInInterface {
  email: string;
  password: string;
}