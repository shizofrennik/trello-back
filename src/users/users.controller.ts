import { Controller, Get, Delete, Param, UseGuards } from '@nestjs/common';
import { ApiUseTags } from '@nestjs/swagger'
import { UsersService } from './users.service';
import { AuthGuard } from '@nestjs/passport';

@Controller('users')
// @ApiUseTags('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  // @Get()
  // @UseGuards(AuthGuard())
  // findAll() {
  //   return [];
  // }

  // @Delete(':userId')
  // delete( @Param('userId') userId) {
  //   return this.usersService.deleteOne(userId);
  // }
}
