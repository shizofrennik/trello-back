import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Users } from './users.entity';
import { Columns } from '../columns/columns.entity';
import { SHA256 } from 'crypto-js';
import { SignUpInterface, SignInInterface } from './users.interface'

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(Users) private readonly usersRepository: Repository<Users>,
    @InjectRepository(Columns) private readonly columnsRepository: Repository<Columns>,
  ) {}

  async findOneByToken(token: string): Promise<Users> {
    try {
      return await this.usersRepository.findOne({ token });
    } catch (err) {
      return err;
    }
  }

  async findAll(): Promise<Users[]> {
    try {
      return await this.usersRepository.find();
    } catch (err) {
      return err;
    }
  }

  async signUp(credentials: SignUpInterface) {
    try {
      const user = {
        ...credentials,
        password: SHA256(credentials.password).toString(),
        token: SHA256(credentials.email).toString(),
        columns: [
          {title: 'BACKLOG'},
          {title: 'TODO'},
          {title: 'IN PROGRESS'},
          {title: 'DONE'}
        ]
      }

      return await this.usersRepository.save(user);
    } catch (err) {
      return err;
    }
  }

  async signIn(credentials: SignInInterface) {
    try {
      const user = {
        ...credentials,
        password: SHA256(credentials.password).toString(),
      }

      return await this.usersRepository.findOneOrFail(user)
    } catch (err) {
      return err;
    }
  }

  async create(entry: Users): Promise<Users> {
    try {
      return await this.usersRepository.save(entry);
    } catch (err) {
      return err;
    }
  }

  async deleteOne(userId: string) {
    try {
      return await this.usersRepository.delete(userId);
    } catch (err) {
      return err;
    }
  }
}