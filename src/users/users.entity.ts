import { Entity, Column, Unique, PrimaryGeneratedColumn, Index, OneToMany } from 'typeorm';
import { Columns } from '../columns/columns.entity';
import { Comments } from '../comments/comments.entity';

@Entity()
@Unique(['email', 'token'])
export class Users {
  @PrimaryGeneratedColumn()
  id: number;

  @Index()
  @Column({ length: 100 })
  email: string;

  @Column({ length: 100, select: false })
  password: string;

  @Column({ length: 100 })
  name: string;

  @Index()
  @Column({ length: 255 })
  token: string;

  @OneToMany(type => Columns, column => column.user, { cascade: true })
  columns: Columns[]

  @OneToMany(type => Comments, comments => comments.user, { cascade: true })
  comments: Comments[]
}