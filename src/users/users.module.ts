import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { Users } from './users.entity';
import { ColumnsService } from '../columns/columns.service';
import { Columns } from '../columns/columns.entity';
import { Cards } from '../cards/cards.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Users, Columns, Cards])],
  controllers: [UsersController],
  providers: [UsersService, ColumnsService],
  exports: [UsersService]
})
export class UsersModule {}
