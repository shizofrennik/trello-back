import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, RelationId } from 'typeorm';
import { Cards } from '../cards/cards.entity';
import { Users } from '../users/users.entity';

@Entity()
export class Comments {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('text')
  body: string;

  @Column('timestamp', { precision: 3, default: () => "CURRENT_TIMESTAMP(3)"})
  created: string;

  @ManyToOne(type => Cards, card => card.comments, { onDelete: 'CASCADE' })
  card: Cards

  @RelationId((comment: Comments) => comment.card)
  cardId: number;

  @ManyToOne(type => Users, user => user.comments, { onDelete: 'CASCADE' })
  user: Users;

  @RelationId((comment: Comments) => comment.user)
  userId: number;
}
