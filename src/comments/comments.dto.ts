import { ApiModelProperty } from '@nestjs/swagger';
import { Cards } from '../cards/cards.entity';
import { Users } from '../users/users.entity';

export class CommentsDto {
  readonly id: number;

  @ApiModelProperty()
  readonly body: string;

  @ApiModelProperty()
  readonly created: string;

  readonly card: Cards;
  readonly cardId: number;
  readonly user: Users;
  readonly userId: number;
}