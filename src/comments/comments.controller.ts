import { Controller, Get, Post, Put, Delete, Body, Param, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags, ApiOperation } from '@nestjs/swagger';
import { CommentsDto } from './comments.dto';
import { CommentsService } from './comments.service';
import { IsCommentOwner } from '../common/guards';
import { Owner } from '../common/reflectors';

@Controller('comments')
@ApiBearerAuth()
@ApiUseTags('comments')
@UseGuards(AuthGuard('bearer'), IsCommentOwner)
export class CommentsController {
  constructor(private readonly commentsService: CommentsService) {}

  @Get()
  @ApiOperation({title: 'Get all comments'})
  async findColumns(@Request() req) {
    return this.commentsService.findUserComments(req.user);
  }

  @Get(':commentId')
  @ApiOperation({title: 'Get comment by id'})
  @Owner(true)
  async findById(@Param('commentId') commentId) {
    return await this.commentsService.find(commentId)
  }

  @Put(':commentId')
  @ApiOperation({title: 'Update comment by id'})
  @Owner(true)
  async update(@Param('commentId') commentId, @Body() commentsDto: CommentsDto) {
    return await this.commentsService.update(commentId, commentsDto)
  }

  @Post()
  @ApiOperation({title: 'Create comment'})
  async create(@Body() commentsDto: CommentsDto, @Request() req) {
    return await this.commentsService.create({...commentsDto, user: req.user});
  }

  @Delete(':commentId')
  @ApiOperation({title: 'Delete comment by id'})
  @Owner(true)
  delete( @Param('commentId') commentId) {
    return this.commentsService.deleteOne(commentId);
  }
}
