import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Comments } from './comments.entity';
import { CommentsDto } from './comments.dto';

@Injectable()
export class CommentsService {
  constructor(
    @InjectRepository(Comments)
    private readonly commentsRepository: Repository<Comments>,
  ) {}

  async findUserComments(user: number): Promise<Comments[]> {
    try {
      return await this.commentsRepository.find({ where: { user }});
    } catch (err) {
      return err;
    }
  }

  async find(id: number): Promise<Comments> {
    try {
      return await this.commentsRepository.findOne(id);
    } catch (err) {
      return err;
    }
  }

  async update(columnId: number, entry: CommentsDto): Promise<Comments> {
    try {
      await this.commentsRepository.update(columnId, {...entry, id: columnId});
      return this.commentsRepository.findOne(columnId);
    } catch (err) {
      return err;
    }
  }

  async create(entry: CommentsDto): Promise<Comments> {
    try {
      return await this.commentsRepository.save({...entry, id: null});
    } catch (err) {
      return err;
    }
  }

  async deleteOne(columnId: string) {
    try {
      return await this.commentsRepository.delete(columnId);
    } catch (err) {
      return err;
    }
  }
}