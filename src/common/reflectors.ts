import { ReflectMetadata } from '@nestjs/common';

export const Owner = (isOwner: boolean) => ReflectMetadata('owner', isOwner);