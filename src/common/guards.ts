import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { ColumnsService } from '../columns/columns.service';
import { CardsService } from '../cards/cards.service';
import { CommentsService } from '../comments/comments.service';

@Injectable()
export class IsColumnOwner implements CanActivate {
  constructor(
    private readonly reflector: Reflector,
    private readonly columnsService: ColumnsService,
  ) {}

  async canActivate(context: ExecutionContext) {
    const request = context.switchToHttp().getRequest();
    const { columnId } = request.params;
    if (columnId) request.column = await this.columnsService.find(columnId);

    const owner = this.reflector.get<boolean>('owner', context.getHandler());

    if (owner && request.column.userId !== request.user.id) return false;

    return true;
  }
}

@Injectable()
export class IsCardOwner implements CanActivate {
  constructor(
    private readonly reflector: Reflector,
    private readonly cardsService: CardsService,
    private readonly columnsService: ColumnsService,
  ) {}

  async canActivate(context: ExecutionContext) {
    const request = context.switchToHttp().getRequest();
    const { cardId } = request.params;
    const owner = this.reflector.get<boolean>('owner', context.getHandler());

    if (!owner) return true;

    const card = await this.cardsService.find(cardId);
    const column = await this.columnsService.find(card.columnId)

    if (owner && column.userId !== request.user.id) return false;

    return true;
  }
}

@Injectable()
export class IsCommentOwner implements CanActivate {
  constructor(
    private readonly reflector: Reflector,
    private readonly commentsService: CommentsService,
  ) {}

  async canActivate(context: ExecutionContext) {
    const request = context.switchToHttp().getRequest();
    const { commentId } = request.params;
    const owner = this.reflector.get<boolean>('owner', context.getHandler());

    const comment = await this.commentsService.find(commentId);

    if (owner && comment.userId !== request.user.id) return false;

    return true;
  }
}