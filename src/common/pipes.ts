import { PipeTransform, Injectable, ArgumentMetadata } from '@nestjs/common';

@Injectable()
export class ReqPipe implements PipeTransform {
  transform(value: any, metadata: ArgumentMetadata) {
    console.log('request: ', value);
    console.log('request: ', metadata.metatype);
    return value;
  }
}